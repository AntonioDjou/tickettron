using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BigSpinScriptOld : MonoBehaviour
{
    public Button lowButton;
    public Button highButton;
    public Animator animController; // To use the animation for rolling
    
    char[] numberSlot = {'0', '1', '2', '3','4','5','6','7','8','9'}; // Digits for Big Spin
    // Update the text on Unity's UI
    public TMPro.TMP_Text D1num; 
    public TMPro.TMP_Text D2num;
    public TMPro.TMP_Text D3num;
    public TMPro.TMP_Text D4num;
    public TMPro.TMP_Text D0num;
    
    //Numbers to compare if the player decision an the random value generated are alligned
    int chosenNumber;
    int randomNumber;
    string randomText;

    bool isLow;
    bool isHigh;

    AudioSource audioSound;
    public AudioSource correctSound;
    public AudioSource wrongSound;
    public AudioSource exchangeSound;
    public AudioSource exchangeFailSound;
    public AudioSource rollSound;
    

    // The points for the player's in-game wallet.
    public static int tronTickets = 5;
    public static int spinTickets = 5;
    public static int winTickets = 5;


    // To update the text referring to the points on the Game's UI.
    public TMPro.TMP_Text tronTicketsText;
    public TMPro.TMP_Text spinTicketsText;
    public TMPro.TMP_Text winTicketsText;
    
    public Image ExchangedTrontoSpinImage;
    public Image ExchangedWintoSpinImage;
    public Image ExchangedWintoTronImage;

    public Image FailedExchangeTrontoSpinImage;
    public Image FailedExchangeWintoSpinImage;
    public Image FailedExchangeWintoTronImage;

    public Button spinButton;
    public Button walletButton;
    public Button myTicketsButton;

    public Image notEnoughSpinPointsImage;

    void Awake()
    {
        audioSound = GetComponent<AudioSource>();
        animController = GetComponent<Animator>();
        winTicketsText.text = Convert.ToString(winTickets);
        spinTicketsText.text = Convert.ToString(spinTickets);
        tronTicketsText.text = Convert.ToString(tronTickets);
    }

    public void Low()
    {
        if(spinTickets > 1)
        {
            spinTickets--;
            spinTicketsText.text = Convert.ToString(spinTickets); // Updates the value on the game's UI
            isLow = true;
            chosenNumber = 4000;

            //animController.SetBool ("isWaiting", false);
            //animController.SetBool ("Digit1Rolling", true); // Start the animation for rolling the first digit
            Digit1Spin();    
        }
        else
        {
            notEnoughSpinPointsImage.gameObject.SetActive(true);
        }
    }

    public void High()
    {
        if(spinTickets < 1)
        {
            notEnoughSpinPointsImage.gameObject.SetActive(true);
        }
        else
        {
            spinTickets--;
            spinTicketsText.text = Convert.ToString(spinTickets);            
            isHigh = true;
            chosenNumber = 5000;

            //animController.SetBool ("isWaiting", false);
            //animController.SetBool ("Digit1Rolling", true);
            Digit1Spin();
        }
    }

    public void Digit1Spin()
    {
        StartCoroutine(D1Roll());
        Invoke("Digit2Spin", 1.4f);
    }

    IEnumerator D1Roll()
    {
        audioSound = rollSound;
        audioSound.Play();
        for (int i = 0; i < 30; i++)
        {
            yield return new WaitForSecondsRealtime(0.02f);
            D1num.text = numberSlot [UnityEngine.Random.Range(0, numberSlot.Length)].ToString(); // Generates 6 different numbers to update on the game's UI while rolling
        }
    }

    public void Digit2Spin()
    {
        //animController.SetBool ("Digit1Rolling", false);
        //animController.SetBool ("Digit2Rolling", true);
        StartCoroutine(D2Roll());
        Invoke("Digit3Spin", 1.4f);
    }
   IEnumerator D2Roll()
    {
        audioSound = rollSound;
        audioSound.Play();
        for (int i = 0; i < 30; i++)
        {
            yield return new WaitForSecondsRealtime(0.02f);
            D2num.text = numberSlot [UnityEngine.Random.Range(0, numberSlot.Length)].ToString();
        }
    }
    public void Digit3Spin()
    {
        //animController.SetBool ("Digit2Rolling", false);
        //animController.SetBool ("Digit3Rolling", true);   
        StartCoroutine(D3Roll());
        Invoke("Digit4Spin", 1.4f);    
    }
    IEnumerator D3Roll()
    {
        audioSound = rollSound;
        audioSound.Play();
        for (int i = 0; i < 30; i++)
        {
            yield return new WaitForSecondsRealtime(0.02f);
            D3num.text = numberSlot [UnityEngine.Random.Range(0, numberSlot.Length)].ToString();
        }
    }
    public void Digit4Spin()
    {
        //animController.SetBool ("Digit3Rolling", false);
        //animController.SetBool ("Digit4Rolling", true);
        StartCoroutine(D4Roll());
        Invoke("Digit0Spin", 1.4f);
    }
    IEnumerator D4Roll()
    {
        audioSound = rollSound;
        audioSound.Play();
        for (int i = 0; i < 30; i++)
        {
            yield return new WaitForSecondsRealtime(0.02f);
            D4num.text = numberSlot [UnityEngine.Random.Range(0, numberSlot.Length)].ToString();
        }
    }

    public void Digit0Spin()
    {
        //animController.SetBool ("Digit4Rolling", false);
        //animController.SetBool ("Digit0Rolling", true);
        StartCoroutine(D0Roll());
        Invoke("CheckConditions", 1.4f);
    }
    IEnumerator D0Roll()
    {
        audioSound = rollSound;
        audioSound.Play();
        for (int i = 0; i < 12; i++)
        {
            yield return new WaitForSecondsRealtime(0.1f);
            D0num.text = numberSlot [UnityEngine.Random.Range(0, 0)].ToString();
        }
    }

    private void CheckConditions()
    {
        //animController.SetBool ("Digit0Rolling", false);
        //animController.SetBool ("isWaiting", true); // Stops the rolling animation
        
        randomText = D0num.text + D4num.text + D3num.text + D2num.text + D1num.text;
        randomNumber = Convert.ToInt32(randomText);
        
        //Winning or Losing conditions
        if((randomNumber > chosenNumber) && (isLow == true) || (randomNumber < chosenNumber) && (isHigh == true)) YouLose();
        else if((randomNumber < chosenNumber) && (isLow == true) || (randomNumber > chosenNumber) && (isHigh == true)) YouWin();
        
        myTicketsButton.gameObject.SetActive(true);
        walletButton.gameObject.SetActive(true);

        lowButton.gameObject.SetActive(true);
        highButton.gameObject.SetActive(true);
        spinButton.gameObject.SetActive(true);
        
        isLow = false;
        isHigh = false;
    }
    public void YouWin()
    {
        audioSound = correctSound;
        audioSound.Play(0);
        animController.SetBool ("isGlowing", true);

        winTickets++;
        winTicketsText.text = Convert.ToString(winTickets);
        StartCoroutine(GlowOff());
    }

    IEnumerator GlowOff()
    {
        yield return new WaitForSecondsRealtime(1f);
        animController.SetBool("isGlowing", false);
    }

    public void YouLose()
    {
        audioSound = wrongSound;
        audioSound.Play(0);
    }

    public void ExchangeTrontoSpin()
    {
        if(tronTickets >= 1)
        {
            tronTickets -= 1;
            spinTickets += 10;
            audioSound = exchangeSound;
            audioSound.Play();
            
            tronTicketsText.text = Convert.ToString(tronTickets);
            spinTicketsText.text = Convert.ToString(spinTickets);
           
            lowButton.gameObject.SetActive(true);
            highButton.gameObject.SetActive(true);

            ExchangedTrontoSpinImage.gameObject.SetActive(true);
        }
        else
        {
            FailedExchangeTrontoSpinImage.gameObject.SetActive(true);
            audioSound = exchangeFailSound;
            audioSound.Play();
        }     
    }

    public void ExchangeWintoSpin()
    {
        if(winTickets >= 1)
        {
            winTickets -= 1;
            spinTickets += 2;
            audioSound = exchangeSound;
            audioSound.Play();
            
            winTicketsText.text = Convert.ToString(winTickets);
            spinTicketsText.text = Convert.ToString(spinTickets);

            if(spinTickets >=1)
            {
                lowButton.gameObject.SetActive(true);
                highButton.gameObject.SetActive(true);
            }

            ExchangedWintoSpinImage.gameObject.SetActive(true);
        }
        else
        {
            FailedExchangeWintoSpinImage.gameObject.SetActive(true);
            audioSound = exchangeFailSound;
            audioSound.Play();
        }    
    }

    public void ExchangeWintoTron()
    {
        if(winTickets >= 2)
        {
            winTickets -= 2;
            tronTickets += 1;
            audioSound = exchangeSound;
            audioSound.Play();
            
            winTicketsText.text = Convert.ToString(winTickets);
            tronTicketsText.text = Convert.ToString(tronTickets);

            ExchangedWintoTronImage.gameObject.SetActive(true);
        }
        else
        {
            FailedExchangeWintoTronImage.gameObject.SetActive(true);
            audioSound = exchangeFailSound;
            audioSound.Play();
        } 
    }
}