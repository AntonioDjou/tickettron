using UnityEngine;
using TMPro;

public class TicketsLayout : MonoBehaviour
{
    private static int ticketsCount = 0;
    private static string ticketsNumb; // The numbers on the ticket for the lottery check
    GameObject ticketTemplate;
    GameObject generatedTicket;
    bool firstSpin=true;
    
    public void GenerateTicket()
    {
        ticketTemplate = transform.GetChild (0).gameObject;
        ticketsCount = SuperSpinScript.ticketAmount;
        ticketsNumb = SuperSpinScript.ticketText;
 
        generatedTicket = Instantiate (ticketTemplate, transform);
        generatedTicket.transform.GetChild(0).GetComponent<TMP_Text>().text = "Ticket #" + (ticketsCount);
        generatedTicket.transform.GetChild(1).GetComponent<TMP_Text>().text = ticketsNumb;

        if(firstSpin == true) 
        {
            Destroy (ticketTemplate);
            firstSpin = false;
        }      
    }   
}
