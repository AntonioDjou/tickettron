using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SuperSpinScript : MonoBehaviour
{
    List <int> lottery = new List <int>();
    List <string> tickets = new List<string> ();
    int index = 0;
    public static int ticketAmount = 0;
    public static string ticketText;

    private static int winTtk;
    public TMPro.TMP_Text winTtkText;
    public TicketsLayout genTicket;
    
    public TMPro.TMP_Text slot1Num;
    public TMPro.TMP_Text slot2Num;
    public TMPro.TMP_Text slot3Num;
    public TMPro.TMP_Text slot4Num;
    public TMPro.TMP_Text slot5Num;
    public TMPro.TMP_Text slot6Num;
    
    public TMPro.TMP_Text slot1Copy;
    public TMPro.TMP_Text slot2Copy;
    public TMPro.TMP_Text slot3Copy;
    public TMPro.TMP_Text slot4Copy;
    public TMPro.TMP_Text slot5Copy;
    public TMPro.TMP_Text slot6Copy;

    //public TMPro.TMP_Text SuperSpinPot; 
    
    public Button spinButton;
    public Button highButton;
    public Button lowButton;
   
    public Button walletButton;
    public Button myTicketsButton;
    public Image notEnoughWinPointsImage;

    AudioSource soundEffect;
    public AudioSource superSpinSound;
    public AudioSource ticketSound;

    void Awake ()
    {
        soundEffect = GetComponent<AudioSource>();

        slot1Num.enabled = false;
        slot2Num.enabled = false;
        slot3Num.enabled = false;
        slot4Num.enabled = false;
        slot5Num.enabled = false;
        slot6Num.enabled = false;

        slot1Copy.enabled = true;
        slot2Copy.enabled = true;
        slot3Copy.enabled = true;
        slot4Copy.enabled = true;
        slot5Copy.enabled = true;
        slot6Copy.enabled = true;
    }
    void Update()
    {
        winTtk = BigSpinScript.winTickets;
        slot1Num.text = (UnityEngine.Random.Range(1, 50)).ToString("D2");
        slot2Num.text = slot1Num.text;
        slot3Num.text = slot1Num.text;
        slot4Num.text = slot1Num.text;
        slot5Num.text = slot1Num.text;
        slot6Num.text = slot1Num.text;
    }
    
    public void Slot1Enabled()
    {
        if(winTtk <= 1)
        {
            notEnoughWinPointsImage.gameObject.SetActive(true);
        }
        else
        {
            lowButton.gameObject.SetActive(false);
            highButton.gameObject.SetActive(false);
            walletButton.gameObject.SetActive(false);
            
            winTtk -= 2;
            BigSpinScript.winTickets = winTtk;
            winTtkText.text = Convert.ToString(winTtk);
           
            slot1Copy.enabled = false;
            slot2Copy.enabled = false;
            slot3Copy.enabled = false;
            slot4Copy.enabled = false;
            slot5Copy.enabled = false;
            slot6Copy.enabled = false;

            slot1Num.enabled = true;
            slot2Num.enabled = true;        
            slot3Num.enabled = true;
            slot4Num.enabled = true;
            slot5Num.enabled = true;
            slot6Num.enabled = true;

            int rand = 0;
            for (int i = 0; i < 6; i++)
            {
                do
                {
                    rand = UnityEngine.Random.Range(1,50); // Generates a random number
                }
                while (lottery.Contains(rand)); // Checks if the generated number is not repeated
                {
                    lottery.Add(rand); // Adds it to the list
                }    
            }
            Invoke("DelayNum1", 1f);
        }
    }
    void DelayNum1()
    {
        //soundEffect = superSpinSound;
        //soundEffect.Play();

        tickets.Add ("");
        slot1Copy.text = lottery[0].ToString("D2");
        slot1Num.enabled = false;
        slot1Copy.enabled = true;
        tickets[index] += slot1Copy.text;
        Invoke("DelayNum2", 1f);
    }
    void DelayNum2()
    {
        //soundEffect = superSpinSound;
        //soundEffect.Play();

        slot2Copy.text = lottery[1].ToString("D2");
        slot2Num.enabled = false;
        slot2Copy.enabled = true;
        tickets[index] += " " + slot2Copy.text;
        Invoke("DelayNum3", 1f);
    }

    void DelayNum3()
    {
        //soundEffect = superSpinSound;
        //soundEffect.Play();

        slot3Copy.text = lottery[2].ToString("D2");
        slot3Num.enabled = false;
        slot3Copy.enabled = true;
        tickets[index] += " " + slot3Copy.text;
        Invoke("DelayNum4", 1f);
    }
    void DelayNum4()
    {
        //soundEffect = superSpinSound;
        //soundEffect.Play();

        slot4Copy.text = lottery[3].ToString("D2");
        slot4Num.enabled = false;
        slot4Copy.enabled = true;
        tickets[index] += " " + slot4Copy.text;
        Invoke("DelayNum5", 1f);
    }
    void DelayNum5()
    {
        //soundEffect = superSpinSound;
        //soundEffect.Play();

        slot5Copy.text = lottery[4].ToString("D2");
        slot5Num.enabled = false;
        slot5Copy.enabled = true;
        tickets[index] += " " + slot5Copy.text;
        Invoke("DelayNum6", 1f);
    }
    void DelayNum6()
    {
        slot6Copy.text = lottery[5].ToString("D2");
        slot6Num.enabled = false;
        slot6Copy.enabled = true;
        tickets[index] += " " + slot6Copy.text;
        //ticketText = tickets[index];
        //Debug.Log(slot6Copy.text + " " + slot5Copy.text + " " + slot4Copy.text + " " + slot3Copy.text + " " + slot2Copy.text + " " + slot1Copy.text);
        ticketText = slot6Copy.text + " " + slot5Copy.text + " " + slot4Copy.text + " " + slot3Copy.text + " " + slot2Copy.text + " " + slot1Copy.text;
        
        lottery = new List <int>(); // Clears the lottery list
        index++;
        ticketAmount++;
        
        walletButton.gameObject.SetActive(true);
        myTicketsButton.gameObject.SetActive(true);

        spinButton.gameObject.SetActive(true);        
        lowButton.gameObject.SetActive(true);
        highButton.gameObject.SetActive(true);

        genTicket.GenerateTicket();
        soundEffect = ticketSound;
        soundEffect.Play();
    }
}