using UnityEngine;
using UnityEngine.UI;

public class RulesScript : MonoBehaviour
{
    public Image rulesImage;

    void Start()
    {
        if (PlayerPrefs.GetInt("FIRSTTIMEOPENING", 1) == 1)
        {
            Debug.Log("First Time Opening the game.");

            //Set first time opening to false
            PlayerPrefs.SetInt("FIRSTTIMEOPENING", 0);
            //Do your stuff here
            rulesImage.gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("NOT first time!");
        }  
    }
}
