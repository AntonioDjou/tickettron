
namespace frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter
{
    public class BaseItemViewsHolder
    {
       public int itemIndex;
       public UnityEngine.RectTransform root;

       internal float cachedSize;

       public virtual void CollectViews()
       { }
    }
}