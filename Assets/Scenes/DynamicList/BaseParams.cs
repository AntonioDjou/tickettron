using System;
using UnityEngine;
using UnityEngine.UI;

namespace frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter
{
    [System.Serializable]
    public class BaseParams
    {
        public ScrollRect scrollRect;
        public RectTransform viewport;
        public RectTransform content;

        public BaseParams() { }

        public BaseParams(BaseParams other)
        {
            this.scrollRect = other.scrollRect;
            this.viewport = other.viewport;
            this.content = other.content;
        }    
        
        public BaseParams(ScrollRect scrollRect, RectTransform viewport, RectTransform content, int itemsCount)
        {
            this.scrollRect = scrollRect;
            this.viewport = viewport;
            this.content = content;
        }

        /*public float GetAbstractNormalizedScrollPosition()
        {

        }*/
    }
}