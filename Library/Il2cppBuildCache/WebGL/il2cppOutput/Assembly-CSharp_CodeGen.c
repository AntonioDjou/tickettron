﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BigSpinScript::Awake()
extern void BigSpinScript_Awake_mC6C4C5C078D6ACEA7EFE8AE51828BE21A09F20B4 (void);
// 0x00000002 System.Void BigSpinScript::Low()
extern void BigSpinScript_Low_m120332FA8FCED98E4107A9F15ED3E7312460B7C9 (void);
// 0x00000003 System.Void BigSpinScript::High()
extern void BigSpinScript_High_m0620CD86F64A0195645C8C4C0354E2EA1E3D1EED (void);
// 0x00000004 System.Void BigSpinScript::Digit1Spin()
extern void BigSpinScript_Digit1Spin_mC6BAA35ADCD0122B51A5CB97B9512CE5CBC5A5BE (void);
// 0x00000005 System.Collections.IEnumerator BigSpinScript::D1Roll()
extern void BigSpinScript_D1Roll_mCA04F29B0D89B938CD0BAAC1DFA93BE0F3F5B93E (void);
// 0x00000006 System.Void BigSpinScript::Digit2Spin()
extern void BigSpinScript_Digit2Spin_mBD5FE26D4D55A662B48C422FF67C78EC340F44FB (void);
// 0x00000007 System.Collections.IEnumerator BigSpinScript::D2Roll()
extern void BigSpinScript_D2Roll_m6DF26307F1F6C8FA0639ACE4089FD048F6BF9A2B (void);
// 0x00000008 System.Void BigSpinScript::Digit3Spin()
extern void BigSpinScript_Digit3Spin_m33BA57D2F34A2F56D709E6A58968094EE4A886C1 (void);
// 0x00000009 System.Collections.IEnumerator BigSpinScript::D3Roll()
extern void BigSpinScript_D3Roll_m9B6955F3BB90BFDDE56D91D87B402A5DE1EE5363 (void);
// 0x0000000A System.Void BigSpinScript::Digit4Spin()
extern void BigSpinScript_Digit4Spin_mD922A1CD3E95B18250569B14A18B5DF7471113EA (void);
// 0x0000000B System.Collections.IEnumerator BigSpinScript::D4Roll()
extern void BigSpinScript_D4Roll_m2B33EA25D83B1841421844288BE5697BA8617C42 (void);
// 0x0000000C System.Void BigSpinScript::Digit0Spin()
extern void BigSpinScript_Digit0Spin_mC523B8D8224DE710B0A67C8D94C56FF0749218B3 (void);
// 0x0000000D System.Collections.IEnumerator BigSpinScript::D0Roll()
extern void BigSpinScript_D0Roll_m5F8F5EB69DDAE7E2FF6FE27AF4088F801F9390D0 (void);
// 0x0000000E System.Void BigSpinScript::CheckConditions()
extern void BigSpinScript_CheckConditions_mAAA681CF14EBBDB6034E95A8E80A08D124CA78AF (void);
// 0x0000000F System.Void BigSpinScript::YouWin()
extern void BigSpinScript_YouWin_m4A174A3B03E4B84CEB3FF389188CB460CA99A680 (void);
// 0x00000010 System.Collections.IEnumerator BigSpinScript::GlowOff()
extern void BigSpinScript_GlowOff_mE587F6A8033BA8E74F22CD50EB882D452825E1B2 (void);
// 0x00000011 System.Void BigSpinScript::YouLose()
extern void BigSpinScript_YouLose_mBCC13070EB53875ECBFE48AB64B1891E9BFB8A85 (void);
// 0x00000012 System.Void BigSpinScript::ExchangeTRCtoTron()
extern void BigSpinScript_ExchangeTRCtoTron_m168045121AB78FE3BAEED2BA8C4A52726A08F0FA (void);
// 0x00000013 System.Void BigSpinScript::ExchangeTrontoSpin()
extern void BigSpinScript_ExchangeTrontoSpin_m7C6176837420290226450B117451EDFAD34A77E8 (void);
// 0x00000014 System.Void BigSpinScript::ExchangeWintoSpin()
extern void BigSpinScript_ExchangeWintoSpin_m6B719C116E0643B9354203CD0E1D5D53E2AC5C04 (void);
// 0x00000015 System.Void BigSpinScript::ExchangeWintoTron()
extern void BigSpinScript_ExchangeWintoTron_mF28A261FA262EDE5E51B7840EEE2865882324D52 (void);
// 0x00000016 System.Void BigSpinScript::ExchangeTrontoTRC()
extern void BigSpinScript_ExchangeTrontoTRC_m07D5E1C86FDC267D78594690D2B15C224478B6B7 (void);
// 0x00000017 System.Void BigSpinScript::.ctor()
extern void BigSpinScript__ctor_mD06B7425AA5C558B60932E194093E63573881279 (void);
// 0x00000018 System.Void BigSpinScript::.cctor()
extern void BigSpinScript__cctor_m62F4A12B0D86E72BCC4AEB161019EA07208EB8DB (void);
// 0x00000019 System.Void BigSpinScript/<D1Roll>d__46::.ctor(System.Int32)
extern void U3CD1RollU3Ed__46__ctor_mC43CF925E71EEEE6D5EC6B7B60A1FEB51CB7412C (void);
// 0x0000001A System.Void BigSpinScript/<D1Roll>d__46::System.IDisposable.Dispose()
extern void U3CD1RollU3Ed__46_System_IDisposable_Dispose_m1737693F7494C54CCB147B2C7D01AB676A313302 (void);
// 0x0000001B System.Boolean BigSpinScript/<D1Roll>d__46::MoveNext()
extern void U3CD1RollU3Ed__46_MoveNext_m2F5C5B9AF870D9D9601D992F5F94082B54076EF3 (void);
// 0x0000001C System.Object BigSpinScript/<D1Roll>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD1RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25CC43358CCA05CD90AC1552FB25C9EA58ABE167 (void);
// 0x0000001D System.Void BigSpinScript/<D1Roll>d__46::System.Collections.IEnumerator.Reset()
extern void U3CD1RollU3Ed__46_System_Collections_IEnumerator_Reset_mD6C98D63697809D196D8C98C660229ED2855F683 (void);
// 0x0000001E System.Object BigSpinScript/<D1Roll>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CD1RollU3Ed__46_System_Collections_IEnumerator_get_Current_m5B2EBA12F80D0C360E48312F206527B26AD7CF2A (void);
// 0x0000001F System.Void BigSpinScript/<D2Roll>d__48::.ctor(System.Int32)
extern void U3CD2RollU3Ed__48__ctor_m34F9DF1FCF70C68F78DF3313A0761A4F1E54DF5B (void);
// 0x00000020 System.Void BigSpinScript/<D2Roll>d__48::System.IDisposable.Dispose()
extern void U3CD2RollU3Ed__48_System_IDisposable_Dispose_m2F082C8FB44BCDCE2A6681CD6B2F1352F6AD3FE9 (void);
// 0x00000021 System.Boolean BigSpinScript/<D2Roll>d__48::MoveNext()
extern void U3CD2RollU3Ed__48_MoveNext_m18DE0E33117BFFC631407351A5F3DB5B20B9FE8E (void);
// 0x00000022 System.Object BigSpinScript/<D2Roll>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD2RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m076D1448F288304DBDEFED7E4F0F9C7C11DC09C1 (void);
// 0x00000023 System.Void BigSpinScript/<D2Roll>d__48::System.Collections.IEnumerator.Reset()
extern void U3CD2RollU3Ed__48_System_Collections_IEnumerator_Reset_m47D792AD225263C1FDD16331EF799DC65A1EF2B1 (void);
// 0x00000024 System.Object BigSpinScript/<D2Roll>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CD2RollU3Ed__48_System_Collections_IEnumerator_get_Current_m141EFC4D4993D6B515760ACC5BC4C6DBBE1EB606 (void);
// 0x00000025 System.Void BigSpinScript/<D3Roll>d__50::.ctor(System.Int32)
extern void U3CD3RollU3Ed__50__ctor_mF664E841A2123E9F87AF022D6B8B2746BB3F5771 (void);
// 0x00000026 System.Void BigSpinScript/<D3Roll>d__50::System.IDisposable.Dispose()
extern void U3CD3RollU3Ed__50_System_IDisposable_Dispose_m2D21D270EA4787BD1F24883F39726A18F1284F32 (void);
// 0x00000027 System.Boolean BigSpinScript/<D3Roll>d__50::MoveNext()
extern void U3CD3RollU3Ed__50_MoveNext_mA7BF3FB00C2A05C72A0D61BBDD9940FFF1324389 (void);
// 0x00000028 System.Object BigSpinScript/<D3Roll>d__50::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD3RollU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m740B9B023DDF18C7EB4E53897699BE7AD4E590C5 (void);
// 0x00000029 System.Void BigSpinScript/<D3Roll>d__50::System.Collections.IEnumerator.Reset()
extern void U3CD3RollU3Ed__50_System_Collections_IEnumerator_Reset_m5934D42F0AFC9E361057795639E9450C6D51EBAB (void);
// 0x0000002A System.Object BigSpinScript/<D3Roll>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CD3RollU3Ed__50_System_Collections_IEnumerator_get_Current_m2875529B1353D233B2EF3053770BDC98121A8D2C (void);
// 0x0000002B System.Void BigSpinScript/<D4Roll>d__52::.ctor(System.Int32)
extern void U3CD4RollU3Ed__52__ctor_m39EA26456538681B5B450FBC0A45048B685EA443 (void);
// 0x0000002C System.Void BigSpinScript/<D4Roll>d__52::System.IDisposable.Dispose()
extern void U3CD4RollU3Ed__52_System_IDisposable_Dispose_mFC12D23B3C5630D31B5E09668CD03DC698664095 (void);
// 0x0000002D System.Boolean BigSpinScript/<D4Roll>d__52::MoveNext()
extern void U3CD4RollU3Ed__52_MoveNext_m96A86206B09B43318C41293F594BA6A9A3BACFA0 (void);
// 0x0000002E System.Object BigSpinScript/<D4Roll>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD4RollU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC687A2B724C9A95AED3B5409F928EFAE5D991FB3 (void);
// 0x0000002F System.Void BigSpinScript/<D4Roll>d__52::System.Collections.IEnumerator.Reset()
extern void U3CD4RollU3Ed__52_System_Collections_IEnumerator_Reset_m199DF633E2126455CD22DA25E135939F99743630 (void);
// 0x00000030 System.Object BigSpinScript/<D4Roll>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CD4RollU3Ed__52_System_Collections_IEnumerator_get_Current_mCD7E9315D3909443AD249ACB90185137E3371D09 (void);
// 0x00000031 System.Void BigSpinScript/<D0Roll>d__54::.ctor(System.Int32)
extern void U3CD0RollU3Ed__54__ctor_m4ED4B2248BD7134DAFB983D43FF6CD0F25D7415E (void);
// 0x00000032 System.Void BigSpinScript/<D0Roll>d__54::System.IDisposable.Dispose()
extern void U3CD0RollU3Ed__54_System_IDisposable_Dispose_mA1C2AC44AD18E50670E13C5A5F13B4D5FA6B911F (void);
// 0x00000033 System.Boolean BigSpinScript/<D0Roll>d__54::MoveNext()
extern void U3CD0RollU3Ed__54_MoveNext_mD001E7C588505E45F7796FEF0A6DD85E9D3CD572 (void);
// 0x00000034 System.Object BigSpinScript/<D0Roll>d__54::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD0RollU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EFBE0283E4E16688B37B5E3CD7C5B6000CBFD19 (void);
// 0x00000035 System.Void BigSpinScript/<D0Roll>d__54::System.Collections.IEnumerator.Reset()
extern void U3CD0RollU3Ed__54_System_Collections_IEnumerator_Reset_m4F484A1845BF4C7E1B1A1E62EAA91F33AD2F7BDB (void);
// 0x00000036 System.Object BigSpinScript/<D0Roll>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CD0RollU3Ed__54_System_Collections_IEnumerator_get_Current_m910B70EA328B0A578960A27CB09F1453BE4EE6ED (void);
// 0x00000037 System.Void BigSpinScript/<GlowOff>d__57::.ctor(System.Int32)
extern void U3CGlowOffU3Ed__57__ctor_m6FCC904E99133E35852DAE3A53C7784718016147 (void);
// 0x00000038 System.Void BigSpinScript/<GlowOff>d__57::System.IDisposable.Dispose()
extern void U3CGlowOffU3Ed__57_System_IDisposable_Dispose_mA4FA503FC230E455AC4732FE3BC893FF4A2BAC5B (void);
// 0x00000039 System.Boolean BigSpinScript/<GlowOff>d__57::MoveNext()
extern void U3CGlowOffU3Ed__57_MoveNext_m5A32A98A63ECF717E1F2BD747E86622E7D021D50 (void);
// 0x0000003A System.Object BigSpinScript/<GlowOff>d__57::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGlowOffU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC66749C532249391E6CDB39582252CF44A2C43A (void);
// 0x0000003B System.Void BigSpinScript/<GlowOff>d__57::System.Collections.IEnumerator.Reset()
extern void U3CGlowOffU3Ed__57_System_Collections_IEnumerator_Reset_m596CC7F9EDC1EFD59D0D8325BCC857D6DE200D6E (void);
// 0x0000003C System.Object BigSpinScript/<GlowOff>d__57::System.Collections.IEnumerator.get_Current()
extern void U3CGlowOffU3Ed__57_System_Collections_IEnumerator_get_Current_m08E7E10DA481D334BC87188BA8ED3D3131F7BC31 (void);
// 0x0000003D System.Void BigSpinScriptOld::Awake()
extern void BigSpinScriptOld_Awake_mFE5BDE39903B1B163EBA4CFC5EE496952D510D02 (void);
// 0x0000003E System.Void BigSpinScriptOld::Low()
extern void BigSpinScriptOld_Low_mCB655AD8F290BA181C4B58D8E3854E8187E79AA6 (void);
// 0x0000003F System.Void BigSpinScriptOld::High()
extern void BigSpinScriptOld_High_m40CE53040B31F51125FE6039A96FFE13E38E1853 (void);
// 0x00000040 System.Void BigSpinScriptOld::Digit1Spin()
extern void BigSpinScriptOld_Digit1Spin_mC89A7251907D754460D4BE7E36AB6E100008B18C (void);
// 0x00000041 System.Collections.IEnumerator BigSpinScriptOld::D1Roll()
extern void BigSpinScriptOld_D1Roll_m36B98E6620CAAFC4743AEAECBADA3E0D9F3481B3 (void);
// 0x00000042 System.Void BigSpinScriptOld::Digit2Spin()
extern void BigSpinScriptOld_Digit2Spin_mC02F818E9E900B65C0FFB44FF4E48023496BF237 (void);
// 0x00000043 System.Collections.IEnumerator BigSpinScriptOld::D2Roll()
extern void BigSpinScriptOld_D2Roll_m1CD4D1AABF35B11A56007ED0FEFDAA6186B3A17F (void);
// 0x00000044 System.Void BigSpinScriptOld::Digit3Spin()
extern void BigSpinScriptOld_Digit3Spin_m24060164B69EF28AEA0FBA98601F40D07E6B9A18 (void);
// 0x00000045 System.Collections.IEnumerator BigSpinScriptOld::D3Roll()
extern void BigSpinScriptOld_D3Roll_m299750C03D7D84B7453F829BE5A4DEF125162237 (void);
// 0x00000046 System.Void BigSpinScriptOld::Digit4Spin()
extern void BigSpinScriptOld_Digit4Spin_m7291834D6430C2170DB83D7D7E834FFC94F3AEA7 (void);
// 0x00000047 System.Collections.IEnumerator BigSpinScriptOld::D4Roll()
extern void BigSpinScriptOld_D4Roll_mB6840223079D89D43E6C3F3088941B7C61F832B4 (void);
// 0x00000048 System.Void BigSpinScriptOld::Digit0Spin()
extern void BigSpinScriptOld_Digit0Spin_mB9E6665FADA874F566F94D83E1D39EACFD840B11 (void);
// 0x00000049 System.Collections.IEnumerator BigSpinScriptOld::D0Roll()
extern void BigSpinScriptOld_D0Roll_m57D44D8EE15528C0492A1FE37FC02A90E34BAFC4 (void);
// 0x0000004A System.Void BigSpinScriptOld::CheckConditions()
extern void BigSpinScriptOld_CheckConditions_mDB7BB5CF1A157930AFD7A159213A3922D132CEE4 (void);
// 0x0000004B System.Void BigSpinScriptOld::YouWin()
extern void BigSpinScriptOld_YouWin_mE8545DE6CDBF25C1B9565203FDBE52759D4592EB (void);
// 0x0000004C System.Collections.IEnumerator BigSpinScriptOld::GlowOff()
extern void BigSpinScriptOld_GlowOff_m2CB22540441C473BDD23D617F05437AB4515D20E (void);
// 0x0000004D System.Void BigSpinScriptOld::YouLose()
extern void BigSpinScriptOld_YouLose_mD5A87997EB6219CE16CB3646746D7DC99064CFF9 (void);
// 0x0000004E System.Void BigSpinScriptOld::ExchangeTrontoSpin()
extern void BigSpinScriptOld_ExchangeTrontoSpin_mD7FB77DD1CA32AC9C84082C3C09370D6022E1F94 (void);
// 0x0000004F System.Void BigSpinScriptOld::ExchangeWintoSpin()
extern void BigSpinScriptOld_ExchangeWintoSpin_m10E8AE2A7ABA8198791E6EC99504AF821B80AC74 (void);
// 0x00000050 System.Void BigSpinScriptOld::ExchangeWintoTron()
extern void BigSpinScriptOld_ExchangeWintoTron_m583F352738807944162096A006C54924569532D5 (void);
// 0x00000051 System.Void BigSpinScriptOld::.ctor()
extern void BigSpinScriptOld__ctor_mBA2712C51EC726FB7F4099E97F057C3F0AE23456 (void);
// 0x00000052 System.Void BigSpinScriptOld::.cctor()
extern void BigSpinScriptOld__cctor_mF713A254A7B534AD9A886D73069C681144915BCA (void);
// 0x00000053 System.Void BigSpinScriptOld/<D1Roll>d__40::.ctor(System.Int32)
extern void U3CD1RollU3Ed__40__ctor_mE5B29F6FEDB78A1E0DFDED44AE446990ADEB1BCF (void);
// 0x00000054 System.Void BigSpinScriptOld/<D1Roll>d__40::System.IDisposable.Dispose()
extern void U3CD1RollU3Ed__40_System_IDisposable_Dispose_mA01E50BE7CA5932FCAB4D9B3925F9C5E31D19B24 (void);
// 0x00000055 System.Boolean BigSpinScriptOld/<D1Roll>d__40::MoveNext()
extern void U3CD1RollU3Ed__40_MoveNext_m766659AC65E5B97CD1FAA7E177E1469040DA3866 (void);
// 0x00000056 System.Object BigSpinScriptOld/<D1Roll>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD1RollU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A8E091583305BE7D0124F3FB1722662A00250B8 (void);
// 0x00000057 System.Void BigSpinScriptOld/<D1Roll>d__40::System.Collections.IEnumerator.Reset()
extern void U3CD1RollU3Ed__40_System_Collections_IEnumerator_Reset_m1BE81F253360CC349978ADF7D371F8781B9759C9 (void);
// 0x00000058 System.Object BigSpinScriptOld/<D1Roll>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CD1RollU3Ed__40_System_Collections_IEnumerator_get_Current_mAB9C9E2BE53BD8720D526B9C3928A7B37DE6CB00 (void);
// 0x00000059 System.Void BigSpinScriptOld/<D2Roll>d__42::.ctor(System.Int32)
extern void U3CD2RollU3Ed__42__ctor_mF657CCBFB546DFE901444AFCD59CA8D0FA503DC6 (void);
// 0x0000005A System.Void BigSpinScriptOld/<D2Roll>d__42::System.IDisposable.Dispose()
extern void U3CD2RollU3Ed__42_System_IDisposable_Dispose_m00ADC5355615BC4F0CEE048646265A9A6310E3B2 (void);
// 0x0000005B System.Boolean BigSpinScriptOld/<D2Roll>d__42::MoveNext()
extern void U3CD2RollU3Ed__42_MoveNext_mD34144C711BD8FA4AD046E6B562293CA459E80DC (void);
// 0x0000005C System.Object BigSpinScriptOld/<D2Roll>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD2RollU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2982DE58894FF46A83F4EDBC671AEA19EFEBB5C4 (void);
// 0x0000005D System.Void BigSpinScriptOld/<D2Roll>d__42::System.Collections.IEnumerator.Reset()
extern void U3CD2RollU3Ed__42_System_Collections_IEnumerator_Reset_m074EF937CFD67DDDC7BC91B0A0AF9502B12656C6 (void);
// 0x0000005E System.Object BigSpinScriptOld/<D2Roll>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CD2RollU3Ed__42_System_Collections_IEnumerator_get_Current_mEA20832653AB5C5F26DA22A008419F6B03409E8A (void);
// 0x0000005F System.Void BigSpinScriptOld/<D3Roll>d__44::.ctor(System.Int32)
extern void U3CD3RollU3Ed__44__ctor_m1D5E94C9DC00E9660C34D0EFAC5C8AE0B58FFA9E (void);
// 0x00000060 System.Void BigSpinScriptOld/<D3Roll>d__44::System.IDisposable.Dispose()
extern void U3CD3RollU3Ed__44_System_IDisposable_Dispose_m619B009D97C8ACA839AF65CF5B6D3CBFD8957B0A (void);
// 0x00000061 System.Boolean BigSpinScriptOld/<D3Roll>d__44::MoveNext()
extern void U3CD3RollU3Ed__44_MoveNext_m97454E8BFF9122AB5D366C8CC34D3A4B000E9E0D (void);
// 0x00000062 System.Object BigSpinScriptOld/<D3Roll>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD3RollU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C56EF30D6C5066EAC90CEAADB3EBC12553D7572 (void);
// 0x00000063 System.Void BigSpinScriptOld/<D3Roll>d__44::System.Collections.IEnumerator.Reset()
extern void U3CD3RollU3Ed__44_System_Collections_IEnumerator_Reset_m3E26F9FC99711AA7C10EF99B01E97DD2AA0D560A (void);
// 0x00000064 System.Object BigSpinScriptOld/<D3Roll>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CD3RollU3Ed__44_System_Collections_IEnumerator_get_Current_m4BA2563E4BF763C5D132FD1BDF20D6B837538635 (void);
// 0x00000065 System.Void BigSpinScriptOld/<D4Roll>d__46::.ctor(System.Int32)
extern void U3CD4RollU3Ed__46__ctor_m1AA15588D444E70BD316D014362378636CCF35BC (void);
// 0x00000066 System.Void BigSpinScriptOld/<D4Roll>d__46::System.IDisposable.Dispose()
extern void U3CD4RollU3Ed__46_System_IDisposable_Dispose_m5819824F19F9D85E08066BC0F0B4C2A5AAD48AB6 (void);
// 0x00000067 System.Boolean BigSpinScriptOld/<D4Roll>d__46::MoveNext()
extern void U3CD4RollU3Ed__46_MoveNext_m58317C0E29DD04B59D114C3986915E3744C91F57 (void);
// 0x00000068 System.Object BigSpinScriptOld/<D4Roll>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD4RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m427EE9DD7919DE8F5DC2B50D56503F38F940676C (void);
// 0x00000069 System.Void BigSpinScriptOld/<D4Roll>d__46::System.Collections.IEnumerator.Reset()
extern void U3CD4RollU3Ed__46_System_Collections_IEnumerator_Reset_mC1B021472447AEB7BF0A196D77586D22ED108BCA (void);
// 0x0000006A System.Object BigSpinScriptOld/<D4Roll>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CD4RollU3Ed__46_System_Collections_IEnumerator_get_Current_mB8486BD58549DF44782F6BFAC395E4217812BD28 (void);
// 0x0000006B System.Void BigSpinScriptOld/<D0Roll>d__48::.ctor(System.Int32)
extern void U3CD0RollU3Ed__48__ctor_m5E16E053DC8BB428FAB155457A9EEECFC9AC4E9A (void);
// 0x0000006C System.Void BigSpinScriptOld/<D0Roll>d__48::System.IDisposable.Dispose()
extern void U3CD0RollU3Ed__48_System_IDisposable_Dispose_m3989A15A58B8D2B164540C77BF26100B11CF5A23 (void);
// 0x0000006D System.Boolean BigSpinScriptOld/<D0Roll>d__48::MoveNext()
extern void U3CD0RollU3Ed__48_MoveNext_m51D25242A32F2E967105E480417A96CA2C21920F (void);
// 0x0000006E System.Object BigSpinScriptOld/<D0Roll>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CD0RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7879742364A48D269280EA4DA4B8DC4FFAED8E5C (void);
// 0x0000006F System.Void BigSpinScriptOld/<D0Roll>d__48::System.Collections.IEnumerator.Reset()
extern void U3CD0RollU3Ed__48_System_Collections_IEnumerator_Reset_m980A8717E95854ACBC300882A4E51CAAD3ECD1B4 (void);
// 0x00000070 System.Object BigSpinScriptOld/<D0Roll>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CD0RollU3Ed__48_System_Collections_IEnumerator_get_Current_m9922F41CE0BBCD38679614BD4B5F54607A4DEB10 (void);
// 0x00000071 System.Void BigSpinScriptOld/<GlowOff>d__51::.ctor(System.Int32)
extern void U3CGlowOffU3Ed__51__ctor_m6D157989D9F27FE90E8B55563A1DD3C251885DED (void);
// 0x00000072 System.Void BigSpinScriptOld/<GlowOff>d__51::System.IDisposable.Dispose()
extern void U3CGlowOffU3Ed__51_System_IDisposable_Dispose_m5C5D08FC31FE0F5C814FDF21F4E12E5F744662CD (void);
// 0x00000073 System.Boolean BigSpinScriptOld/<GlowOff>d__51::MoveNext()
extern void U3CGlowOffU3Ed__51_MoveNext_m4A8A1E9E8AB33F178C51C0704D5D7DBF7F54F284 (void);
// 0x00000074 System.Object BigSpinScriptOld/<GlowOff>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGlowOffU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A9EAEEE4CADF12CE47D3C0BCD8C6078330567B8 (void);
// 0x00000075 System.Void BigSpinScriptOld/<GlowOff>d__51::System.Collections.IEnumerator.Reset()
extern void U3CGlowOffU3Ed__51_System_Collections_IEnumerator_Reset_mD58E627AAE1B363BEF1F32A6E77FD4FB938DFFEC (void);
// 0x00000076 System.Object BigSpinScriptOld/<GlowOff>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CGlowOffU3Ed__51_System_Collections_IEnumerator_get_Current_mD29BC3F96D2B0455A326175D18A124818EFE5CCA (void);
// 0x00000077 System.Void RulesScript::Start()
extern void RulesScript_Start_m123826F775905197FA47728CF3CD8F822C2BDC6B (void);
// 0x00000078 System.Void RulesScript::.ctor()
extern void RulesScript__ctor_mC06E975AB484CD3AC9719EB6EAFA9E8565277D46 (void);
// 0x00000079 System.Void SuperSpinScript::Awake()
extern void SuperSpinScript_Awake_m03D489C377A1967633EBBD09D6E84DE21109E2DF (void);
// 0x0000007A System.Void SuperSpinScript::Update()
extern void SuperSpinScript_Update_m44393348D3BA040530CD966CD3D30D9BBA749DE3 (void);
// 0x0000007B System.Void SuperSpinScript::Slot1Enabled()
extern void SuperSpinScript_Slot1Enabled_mC0AB44F33F3D007A9E75F14A8A1FD53CDF503C06 (void);
// 0x0000007C System.Void SuperSpinScript::DelayNum1()
extern void SuperSpinScript_DelayNum1_mFB21D0B5A9E523321408F92A45BB03215838209E (void);
// 0x0000007D System.Void SuperSpinScript::DelayNum2()
extern void SuperSpinScript_DelayNum2_mC16125CB2C7CC6C69BBC5A00B28044AF851BC1F4 (void);
// 0x0000007E System.Void SuperSpinScript::DelayNum3()
extern void SuperSpinScript_DelayNum3_m4FE31D69B0AFED1A803BBD1B0DDF968E8FC718A1 (void);
// 0x0000007F System.Void SuperSpinScript::DelayNum4()
extern void SuperSpinScript_DelayNum4_mB53A2C85033BBBAF6CAC5C2AB7C674A79DD1B88C (void);
// 0x00000080 System.Void SuperSpinScript::DelayNum5()
extern void SuperSpinScript_DelayNum5_m2D29F8A9BABF73BE940AEBDC69A85EEC750C42D4 (void);
// 0x00000081 System.Void SuperSpinScript::DelayNum6()
extern void SuperSpinScript_DelayNum6_mD1C16C5B5013A7A6A9BD0DC77168D5B7077CA125 (void);
// 0x00000082 System.Void SuperSpinScript::.ctor()
extern void SuperSpinScript__ctor_m44A2C4EEA42632A39D4C01C7DAF64EB49AF40C5A (void);
// 0x00000083 System.Void SuperSpinScript::.cctor()
extern void SuperSpinScript__cctor_mC4F93B52A7407A682C50F8ADC67FDB9C1A021FF9 (void);
// 0x00000084 System.Void TicketsLayout::GenerateTicket()
extern void TicketsLayout_GenerateTicket_m4A02F1C1AEF54224581ECAEAD91AFCED48464424 (void);
// 0x00000085 System.Void TicketsLayout::.ctor()
extern void TicketsLayout__ctor_m05812B9BBEF3F95FA127A67775CE3DF774B63BE5 (void);
// 0x00000086 System.Void TicketsLayout::.cctor()
extern void TicketsLayout__cctor_m60985FF77F31CC166D8EA796F000781A77027A4E (void);
// 0x00000087 System.Void frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter.BaseItemViewsHolder::CollectViews()
extern void BaseItemViewsHolder_CollectViews_mF461BCB508CBE2D52409A5195AFC73C6FF827137 (void);
// 0x00000088 System.Void frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter.BaseItemViewsHolder::.ctor()
extern void BaseItemViewsHolder__ctor_m88E15E8D596293A7B9BE4FD1F2AAD97612AC6891 (void);
// 0x00000089 System.Void frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter.BaseParams::.ctor()
extern void BaseParams__ctor_mD20ADA66AFE00210E6511DD3226F5DE590146C2C (void);
// 0x0000008A System.Void frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter.BaseParams::.ctor(frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter.BaseParams)
extern void BaseParams__ctor_mFC93676B4F80957F3A7396C5D78910BA0B240088 (void);
// 0x0000008B System.Void frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter.BaseParams::.ctor(UnityEngine.UI.ScrollRect,UnityEngine.RectTransform,UnityEngine.RectTransform,System.Int32)
extern void BaseParams__ctor_mE6234CCE85796803BB4639A94D01AA50B73AE4D9 (void);
static Il2CppMethodPointer s_methodPointers[139] = 
{
	BigSpinScript_Awake_mC6C4C5C078D6ACEA7EFE8AE51828BE21A09F20B4,
	BigSpinScript_Low_m120332FA8FCED98E4107A9F15ED3E7312460B7C9,
	BigSpinScript_High_m0620CD86F64A0195645C8C4C0354E2EA1E3D1EED,
	BigSpinScript_Digit1Spin_mC6BAA35ADCD0122B51A5CB97B9512CE5CBC5A5BE,
	BigSpinScript_D1Roll_mCA04F29B0D89B938CD0BAAC1DFA93BE0F3F5B93E,
	BigSpinScript_Digit2Spin_mBD5FE26D4D55A662B48C422FF67C78EC340F44FB,
	BigSpinScript_D2Roll_m6DF26307F1F6C8FA0639ACE4089FD048F6BF9A2B,
	BigSpinScript_Digit3Spin_m33BA57D2F34A2F56D709E6A58968094EE4A886C1,
	BigSpinScript_D3Roll_m9B6955F3BB90BFDDE56D91D87B402A5DE1EE5363,
	BigSpinScript_Digit4Spin_mD922A1CD3E95B18250569B14A18B5DF7471113EA,
	BigSpinScript_D4Roll_m2B33EA25D83B1841421844288BE5697BA8617C42,
	BigSpinScript_Digit0Spin_mC523B8D8224DE710B0A67C8D94C56FF0749218B3,
	BigSpinScript_D0Roll_m5F8F5EB69DDAE7E2FF6FE27AF4088F801F9390D0,
	BigSpinScript_CheckConditions_mAAA681CF14EBBDB6034E95A8E80A08D124CA78AF,
	BigSpinScript_YouWin_m4A174A3B03E4B84CEB3FF389188CB460CA99A680,
	BigSpinScript_GlowOff_mE587F6A8033BA8E74F22CD50EB882D452825E1B2,
	BigSpinScript_YouLose_mBCC13070EB53875ECBFE48AB64B1891E9BFB8A85,
	BigSpinScript_ExchangeTRCtoTron_m168045121AB78FE3BAEED2BA8C4A52726A08F0FA,
	BigSpinScript_ExchangeTrontoSpin_m7C6176837420290226450B117451EDFAD34A77E8,
	BigSpinScript_ExchangeWintoSpin_m6B719C116E0643B9354203CD0E1D5D53E2AC5C04,
	BigSpinScript_ExchangeWintoTron_mF28A261FA262EDE5E51B7840EEE2865882324D52,
	BigSpinScript_ExchangeTrontoTRC_m07D5E1C86FDC267D78594690D2B15C224478B6B7,
	BigSpinScript__ctor_mD06B7425AA5C558B60932E194093E63573881279,
	BigSpinScript__cctor_m62F4A12B0D86E72BCC4AEB161019EA07208EB8DB,
	U3CD1RollU3Ed__46__ctor_mC43CF925E71EEEE6D5EC6B7B60A1FEB51CB7412C,
	U3CD1RollU3Ed__46_System_IDisposable_Dispose_m1737693F7494C54CCB147B2C7D01AB676A313302,
	U3CD1RollU3Ed__46_MoveNext_m2F5C5B9AF870D9D9601D992F5F94082B54076EF3,
	U3CD1RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25CC43358CCA05CD90AC1552FB25C9EA58ABE167,
	U3CD1RollU3Ed__46_System_Collections_IEnumerator_Reset_mD6C98D63697809D196D8C98C660229ED2855F683,
	U3CD1RollU3Ed__46_System_Collections_IEnumerator_get_Current_m5B2EBA12F80D0C360E48312F206527B26AD7CF2A,
	U3CD2RollU3Ed__48__ctor_m34F9DF1FCF70C68F78DF3313A0761A4F1E54DF5B,
	U3CD2RollU3Ed__48_System_IDisposable_Dispose_m2F082C8FB44BCDCE2A6681CD6B2F1352F6AD3FE9,
	U3CD2RollU3Ed__48_MoveNext_m18DE0E33117BFFC631407351A5F3DB5B20B9FE8E,
	U3CD2RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m076D1448F288304DBDEFED7E4F0F9C7C11DC09C1,
	U3CD2RollU3Ed__48_System_Collections_IEnumerator_Reset_m47D792AD225263C1FDD16331EF799DC65A1EF2B1,
	U3CD2RollU3Ed__48_System_Collections_IEnumerator_get_Current_m141EFC4D4993D6B515760ACC5BC4C6DBBE1EB606,
	U3CD3RollU3Ed__50__ctor_mF664E841A2123E9F87AF022D6B8B2746BB3F5771,
	U3CD3RollU3Ed__50_System_IDisposable_Dispose_m2D21D270EA4787BD1F24883F39726A18F1284F32,
	U3CD3RollU3Ed__50_MoveNext_mA7BF3FB00C2A05C72A0D61BBDD9940FFF1324389,
	U3CD3RollU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m740B9B023DDF18C7EB4E53897699BE7AD4E590C5,
	U3CD3RollU3Ed__50_System_Collections_IEnumerator_Reset_m5934D42F0AFC9E361057795639E9450C6D51EBAB,
	U3CD3RollU3Ed__50_System_Collections_IEnumerator_get_Current_m2875529B1353D233B2EF3053770BDC98121A8D2C,
	U3CD4RollU3Ed__52__ctor_m39EA26456538681B5B450FBC0A45048B685EA443,
	U3CD4RollU3Ed__52_System_IDisposable_Dispose_mFC12D23B3C5630D31B5E09668CD03DC698664095,
	U3CD4RollU3Ed__52_MoveNext_m96A86206B09B43318C41293F594BA6A9A3BACFA0,
	U3CD4RollU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC687A2B724C9A95AED3B5409F928EFAE5D991FB3,
	U3CD4RollU3Ed__52_System_Collections_IEnumerator_Reset_m199DF633E2126455CD22DA25E135939F99743630,
	U3CD4RollU3Ed__52_System_Collections_IEnumerator_get_Current_mCD7E9315D3909443AD249ACB90185137E3371D09,
	U3CD0RollU3Ed__54__ctor_m4ED4B2248BD7134DAFB983D43FF6CD0F25D7415E,
	U3CD0RollU3Ed__54_System_IDisposable_Dispose_mA1C2AC44AD18E50670E13C5A5F13B4D5FA6B911F,
	U3CD0RollU3Ed__54_MoveNext_mD001E7C588505E45F7796FEF0A6DD85E9D3CD572,
	U3CD0RollU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EFBE0283E4E16688B37B5E3CD7C5B6000CBFD19,
	U3CD0RollU3Ed__54_System_Collections_IEnumerator_Reset_m4F484A1845BF4C7E1B1A1E62EAA91F33AD2F7BDB,
	U3CD0RollU3Ed__54_System_Collections_IEnumerator_get_Current_m910B70EA328B0A578960A27CB09F1453BE4EE6ED,
	U3CGlowOffU3Ed__57__ctor_m6FCC904E99133E35852DAE3A53C7784718016147,
	U3CGlowOffU3Ed__57_System_IDisposable_Dispose_mA4FA503FC230E455AC4732FE3BC893FF4A2BAC5B,
	U3CGlowOffU3Ed__57_MoveNext_m5A32A98A63ECF717E1F2BD747E86622E7D021D50,
	U3CGlowOffU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC66749C532249391E6CDB39582252CF44A2C43A,
	U3CGlowOffU3Ed__57_System_Collections_IEnumerator_Reset_m596CC7F9EDC1EFD59D0D8325BCC857D6DE200D6E,
	U3CGlowOffU3Ed__57_System_Collections_IEnumerator_get_Current_m08E7E10DA481D334BC87188BA8ED3D3131F7BC31,
	BigSpinScriptOld_Awake_mFE5BDE39903B1B163EBA4CFC5EE496952D510D02,
	BigSpinScriptOld_Low_mCB655AD8F290BA181C4B58D8E3854E8187E79AA6,
	BigSpinScriptOld_High_m40CE53040B31F51125FE6039A96FFE13E38E1853,
	BigSpinScriptOld_Digit1Spin_mC89A7251907D754460D4BE7E36AB6E100008B18C,
	BigSpinScriptOld_D1Roll_m36B98E6620CAAFC4743AEAECBADA3E0D9F3481B3,
	BigSpinScriptOld_Digit2Spin_mC02F818E9E900B65C0FFB44FF4E48023496BF237,
	BigSpinScriptOld_D2Roll_m1CD4D1AABF35B11A56007ED0FEFDAA6186B3A17F,
	BigSpinScriptOld_Digit3Spin_m24060164B69EF28AEA0FBA98601F40D07E6B9A18,
	BigSpinScriptOld_D3Roll_m299750C03D7D84B7453F829BE5A4DEF125162237,
	BigSpinScriptOld_Digit4Spin_m7291834D6430C2170DB83D7D7E834FFC94F3AEA7,
	BigSpinScriptOld_D4Roll_mB6840223079D89D43E6C3F3088941B7C61F832B4,
	BigSpinScriptOld_Digit0Spin_mB9E6665FADA874F566F94D83E1D39EACFD840B11,
	BigSpinScriptOld_D0Roll_m57D44D8EE15528C0492A1FE37FC02A90E34BAFC4,
	BigSpinScriptOld_CheckConditions_mDB7BB5CF1A157930AFD7A159213A3922D132CEE4,
	BigSpinScriptOld_YouWin_mE8545DE6CDBF25C1B9565203FDBE52759D4592EB,
	BigSpinScriptOld_GlowOff_m2CB22540441C473BDD23D617F05437AB4515D20E,
	BigSpinScriptOld_YouLose_mD5A87997EB6219CE16CB3646746D7DC99064CFF9,
	BigSpinScriptOld_ExchangeTrontoSpin_mD7FB77DD1CA32AC9C84082C3C09370D6022E1F94,
	BigSpinScriptOld_ExchangeWintoSpin_m10E8AE2A7ABA8198791E6EC99504AF821B80AC74,
	BigSpinScriptOld_ExchangeWintoTron_m583F352738807944162096A006C54924569532D5,
	BigSpinScriptOld__ctor_mBA2712C51EC726FB7F4099E97F057C3F0AE23456,
	BigSpinScriptOld__cctor_mF713A254A7B534AD9A886D73069C681144915BCA,
	U3CD1RollU3Ed__40__ctor_mE5B29F6FEDB78A1E0DFDED44AE446990ADEB1BCF,
	U3CD1RollU3Ed__40_System_IDisposable_Dispose_mA01E50BE7CA5932FCAB4D9B3925F9C5E31D19B24,
	U3CD1RollU3Ed__40_MoveNext_m766659AC65E5B97CD1FAA7E177E1469040DA3866,
	U3CD1RollU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A8E091583305BE7D0124F3FB1722662A00250B8,
	U3CD1RollU3Ed__40_System_Collections_IEnumerator_Reset_m1BE81F253360CC349978ADF7D371F8781B9759C9,
	U3CD1RollU3Ed__40_System_Collections_IEnumerator_get_Current_mAB9C9E2BE53BD8720D526B9C3928A7B37DE6CB00,
	U3CD2RollU3Ed__42__ctor_mF657CCBFB546DFE901444AFCD59CA8D0FA503DC6,
	U3CD2RollU3Ed__42_System_IDisposable_Dispose_m00ADC5355615BC4F0CEE048646265A9A6310E3B2,
	U3CD2RollU3Ed__42_MoveNext_mD34144C711BD8FA4AD046E6B562293CA459E80DC,
	U3CD2RollU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2982DE58894FF46A83F4EDBC671AEA19EFEBB5C4,
	U3CD2RollU3Ed__42_System_Collections_IEnumerator_Reset_m074EF937CFD67DDDC7BC91B0A0AF9502B12656C6,
	U3CD2RollU3Ed__42_System_Collections_IEnumerator_get_Current_mEA20832653AB5C5F26DA22A008419F6B03409E8A,
	U3CD3RollU3Ed__44__ctor_m1D5E94C9DC00E9660C34D0EFAC5C8AE0B58FFA9E,
	U3CD3RollU3Ed__44_System_IDisposable_Dispose_m619B009D97C8ACA839AF65CF5B6D3CBFD8957B0A,
	U3CD3RollU3Ed__44_MoveNext_m97454E8BFF9122AB5D366C8CC34D3A4B000E9E0D,
	U3CD3RollU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C56EF30D6C5066EAC90CEAADB3EBC12553D7572,
	U3CD3RollU3Ed__44_System_Collections_IEnumerator_Reset_m3E26F9FC99711AA7C10EF99B01E97DD2AA0D560A,
	U3CD3RollU3Ed__44_System_Collections_IEnumerator_get_Current_m4BA2563E4BF763C5D132FD1BDF20D6B837538635,
	U3CD4RollU3Ed__46__ctor_m1AA15588D444E70BD316D014362378636CCF35BC,
	U3CD4RollU3Ed__46_System_IDisposable_Dispose_m5819824F19F9D85E08066BC0F0B4C2A5AAD48AB6,
	U3CD4RollU3Ed__46_MoveNext_m58317C0E29DD04B59D114C3986915E3744C91F57,
	U3CD4RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m427EE9DD7919DE8F5DC2B50D56503F38F940676C,
	U3CD4RollU3Ed__46_System_Collections_IEnumerator_Reset_mC1B021472447AEB7BF0A196D77586D22ED108BCA,
	U3CD4RollU3Ed__46_System_Collections_IEnumerator_get_Current_mB8486BD58549DF44782F6BFAC395E4217812BD28,
	U3CD0RollU3Ed__48__ctor_m5E16E053DC8BB428FAB155457A9EEECFC9AC4E9A,
	U3CD0RollU3Ed__48_System_IDisposable_Dispose_m3989A15A58B8D2B164540C77BF26100B11CF5A23,
	U3CD0RollU3Ed__48_MoveNext_m51D25242A32F2E967105E480417A96CA2C21920F,
	U3CD0RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7879742364A48D269280EA4DA4B8DC4FFAED8E5C,
	U3CD0RollU3Ed__48_System_Collections_IEnumerator_Reset_m980A8717E95854ACBC300882A4E51CAAD3ECD1B4,
	U3CD0RollU3Ed__48_System_Collections_IEnumerator_get_Current_m9922F41CE0BBCD38679614BD4B5F54607A4DEB10,
	U3CGlowOffU3Ed__51__ctor_m6D157989D9F27FE90E8B55563A1DD3C251885DED,
	U3CGlowOffU3Ed__51_System_IDisposable_Dispose_m5C5D08FC31FE0F5C814FDF21F4E12E5F744662CD,
	U3CGlowOffU3Ed__51_MoveNext_m4A8A1E9E8AB33F178C51C0704D5D7DBF7F54F284,
	U3CGlowOffU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A9EAEEE4CADF12CE47D3C0BCD8C6078330567B8,
	U3CGlowOffU3Ed__51_System_Collections_IEnumerator_Reset_mD58E627AAE1B363BEF1F32A6E77FD4FB938DFFEC,
	U3CGlowOffU3Ed__51_System_Collections_IEnumerator_get_Current_mD29BC3F96D2B0455A326175D18A124818EFE5CCA,
	RulesScript_Start_m123826F775905197FA47728CF3CD8F822C2BDC6B,
	RulesScript__ctor_mC06E975AB484CD3AC9719EB6EAFA9E8565277D46,
	SuperSpinScript_Awake_m03D489C377A1967633EBBD09D6E84DE21109E2DF,
	SuperSpinScript_Update_m44393348D3BA040530CD966CD3D30D9BBA749DE3,
	SuperSpinScript_Slot1Enabled_mC0AB44F33F3D007A9E75F14A8A1FD53CDF503C06,
	SuperSpinScript_DelayNum1_mFB21D0B5A9E523321408F92A45BB03215838209E,
	SuperSpinScript_DelayNum2_mC16125CB2C7CC6C69BBC5A00B28044AF851BC1F4,
	SuperSpinScript_DelayNum3_m4FE31D69B0AFED1A803BBD1B0DDF968E8FC718A1,
	SuperSpinScript_DelayNum4_mB53A2C85033BBBAF6CAC5C2AB7C674A79DD1B88C,
	SuperSpinScript_DelayNum5_m2D29F8A9BABF73BE940AEBDC69A85EEC750C42D4,
	SuperSpinScript_DelayNum6_mD1C16C5B5013A7A6A9BD0DC77168D5B7077CA125,
	SuperSpinScript__ctor_m44A2C4EEA42632A39D4C01C7DAF64EB49AF40C5A,
	SuperSpinScript__cctor_mC4F93B52A7407A682C50F8ADC67FDB9C1A021FF9,
	TicketsLayout_GenerateTicket_m4A02F1C1AEF54224581ECAEAD91AFCED48464424,
	TicketsLayout__ctor_m05812B9BBEF3F95FA127A67775CE3DF774B63BE5,
	TicketsLayout__cctor_m60985FF77F31CC166D8EA796F000781A77027A4E,
	BaseItemViewsHolder_CollectViews_mF461BCB508CBE2D52409A5195AFC73C6FF827137,
	BaseItemViewsHolder__ctor_m88E15E8D596293A7B9BE4FD1F2AAD97612AC6891,
	BaseParams__ctor_mD20ADA66AFE00210E6511DD3226F5DE590146C2C,
	BaseParams__ctor_mFC93676B4F80957F3A7396C5D78910BA0B240088,
	BaseParams__ctor_mE6234CCE85796803BB4639A94D01AA50B73AE4D9,
};
static const int32_t s_InvokerIndices[139] = 
{
	2273,
	2273,
	2273,
	2273,
	2211,
	2273,
	2211,
	2273,
	2211,
	2273,
	2211,
	2273,
	2211,
	2273,
	2273,
	2211,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	3629,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	2273,
	2273,
	2273,
	2273,
	2211,
	2273,
	2211,
	2273,
	2211,
	2273,
	2211,
	2273,
	2211,
	2273,
	2273,
	2211,
	2273,
	2273,
	2273,
	2273,
	2273,
	3629,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	1865,
	2273,
	2239,
	2211,
	2273,
	2211,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	2273,
	3629,
	2273,
	2273,
	3629,
	2273,
	2273,
	2273,
	1879,
	452,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	139,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
