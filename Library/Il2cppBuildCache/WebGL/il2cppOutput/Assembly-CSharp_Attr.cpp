﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D1Roll_mCA04F29B0D89B938CD0BAAC1DFA93BE0F3F5B93E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_0_0_0_var), NULL);
	}
}
static void BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D2Roll_m6DF26307F1F6C8FA0639ACE4089FD048F6BF9A2B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_0_0_0_var), NULL);
	}
}
static void BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D3Roll_m9B6955F3BB90BFDDE56D91D87B402A5DE1EE5363(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_0_0_0_var), NULL);
	}
}
static void BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D4Roll_m2B33EA25D83B1841421844288BE5697BA8617C42(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_0_0_0_var), NULL);
	}
}
static void BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D0Roll_m5F8F5EB69DDAE7E2FF6FE27AF4088F801F9390D0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_0_0_0_var), NULL);
	}
}
static void BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_GlowOff_mE587F6A8033BA8E74F22CD50EB882D452825E1B2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_0_0_0_var), NULL);
	}
}
static void U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46__ctor_mC43CF925E71EEEE6D5EC6B7B60A1FEB51CB7412C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_IDisposable_Dispose_m1737693F7494C54CCB147B2C7D01AB676A313302(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25CC43358CCA05CD90AC1552FB25C9EA58ABE167(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_Collections_IEnumerator_Reset_mD6C98D63697809D196D8C98C660229ED2855F683(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_Collections_IEnumerator_get_Current_m5B2EBA12F80D0C360E48312F206527B26AD7CF2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48__ctor_m34F9DF1FCF70C68F78DF3313A0761A4F1E54DF5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_IDisposable_Dispose_m2F082C8FB44BCDCE2A6681CD6B2F1352F6AD3FE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m076D1448F288304DBDEFED7E4F0F9C7C11DC09C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_Collections_IEnumerator_Reset_m47D792AD225263C1FDD16331EF799DC65A1EF2B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_Collections_IEnumerator_get_Current_m141EFC4D4993D6B515760ACC5BC4C6DBBE1EB606(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50__ctor_mF664E841A2123E9F87AF022D6B8B2746BB3F5771(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_IDisposable_Dispose_m2D21D270EA4787BD1F24883F39726A18F1284F32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m740B9B023DDF18C7EB4E53897699BE7AD4E590C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_Collections_IEnumerator_Reset_m5934D42F0AFC9E361057795639E9450C6D51EBAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_Collections_IEnumerator_get_Current_m2875529B1353D233B2EF3053770BDC98121A8D2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52__ctor_m39EA26456538681B5B450FBC0A45048B685EA443(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_IDisposable_Dispose_mFC12D23B3C5630D31B5E09668CD03DC698664095(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC687A2B724C9A95AED3B5409F928EFAE5D991FB3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_Collections_IEnumerator_Reset_m199DF633E2126455CD22DA25E135939F99743630(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_Collections_IEnumerator_get_Current_mCD7E9315D3909443AD249ACB90185137E3371D09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54__ctor_m4ED4B2248BD7134DAFB983D43FF6CD0F25D7415E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_IDisposable_Dispose_mA1C2AC44AD18E50670E13C5A5F13B4D5FA6B911F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EFBE0283E4E16688B37B5E3CD7C5B6000CBFD19(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_Collections_IEnumerator_Reset_m4F484A1845BF4C7E1B1A1E62EAA91F33AD2F7BDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_Collections_IEnumerator_get_Current_m910B70EA328B0A578960A27CB09F1453BE4EE6ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57__ctor_m6FCC904E99133E35852DAE3A53C7784718016147(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_IDisposable_Dispose_mA4FA503FC230E455AC4732FE3BC893FF4A2BAC5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC66749C532249391E6CDB39582252CF44A2C43A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_Collections_IEnumerator_Reset_m596CC7F9EDC1EFD59D0D8325BCC857D6DE200D6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_Collections_IEnumerator_get_Current_m08E7E10DA481D334BC87188BA8ED3D3131F7BC31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D1Roll_m36B98E6620CAAFC4743AEAECBADA3E0D9F3481B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_0_0_0_var), NULL);
	}
}
static void BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D2Roll_m1CD4D1AABF35B11A56007ED0FEFDAA6186B3A17F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_0_0_0_var), NULL);
	}
}
static void BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D3Roll_m299750C03D7D84B7453F829BE5A4DEF125162237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_0_0_0_var), NULL);
	}
}
static void BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D4Roll_mB6840223079D89D43E6C3F3088941B7C61F832B4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_0_0_0_var), NULL);
	}
}
static void BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D0Roll_m57D44D8EE15528C0492A1FE37FC02A90E34BAFC4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_0_0_0_var), NULL);
	}
}
static void BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_GlowOff_m2CB22540441C473BDD23D617F05437AB4515D20E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_0_0_0_var), NULL);
	}
}
static void U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40__ctor_mE5B29F6FEDB78A1E0DFDED44AE446990ADEB1BCF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_IDisposable_Dispose_mA01E50BE7CA5932FCAB4D9B3925F9C5E31D19B24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A8E091583305BE7D0124F3FB1722662A00250B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_Collections_IEnumerator_Reset_m1BE81F253360CC349978ADF7D371F8781B9759C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_Collections_IEnumerator_get_Current_mAB9C9E2BE53BD8720D526B9C3928A7B37DE6CB00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42__ctor_mF657CCBFB546DFE901444AFCD59CA8D0FA503DC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_IDisposable_Dispose_m00ADC5355615BC4F0CEE048646265A9A6310E3B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2982DE58894FF46A83F4EDBC671AEA19EFEBB5C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_Collections_IEnumerator_Reset_m074EF937CFD67DDDC7BC91B0A0AF9502B12656C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_Collections_IEnumerator_get_Current_mEA20832653AB5C5F26DA22A008419F6B03409E8A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44__ctor_m1D5E94C9DC00E9660C34D0EFAC5C8AE0B58FFA9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_IDisposable_Dispose_m619B009D97C8ACA839AF65CF5B6D3CBFD8957B0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C56EF30D6C5066EAC90CEAADB3EBC12553D7572(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_Collections_IEnumerator_Reset_m3E26F9FC99711AA7C10EF99B01E97DD2AA0D560A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_Collections_IEnumerator_get_Current_m4BA2563E4BF763C5D132FD1BDF20D6B837538635(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46__ctor_m1AA15588D444E70BD316D014362378636CCF35BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_IDisposable_Dispose_m5819824F19F9D85E08066BC0F0B4C2A5AAD48AB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m427EE9DD7919DE8F5DC2B50D56503F38F940676C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_Collections_IEnumerator_Reset_mC1B021472447AEB7BF0A196D77586D22ED108BCA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_Collections_IEnumerator_get_Current_mB8486BD58549DF44782F6BFAC395E4217812BD28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48__ctor_m5E16E053DC8BB428FAB155457A9EEECFC9AC4E9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_IDisposable_Dispose_m3989A15A58B8D2B164540C77BF26100B11CF5A23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7879742364A48D269280EA4DA4B8DC4FFAED8E5C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_Collections_IEnumerator_Reset_m980A8717E95854ACBC300882A4E51CAAD3ECD1B4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_Collections_IEnumerator_get_Current_m9922F41CE0BBCD38679614BD4B5F54607A4DEB10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51__ctor_m6D157989D9F27FE90E8B55563A1DD3C251885DED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_IDisposable_Dispose_m5C5D08FC31FE0F5C814FDF21F4E12E5F744662CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A9EAEEE4CADF12CE47D3C0BCD8C6078330567B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_Collections_IEnumerator_Reset_mD58E627AAE1B363BEF1F32A6E77FD4FB938DFFEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_Collections_IEnumerator_get_Current_mD29BC3F96D2B0455A326175D18A124818EFE5CCA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[86] = 
{
	U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator,
	U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator,
	U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator,
	U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator,
	U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator,
	U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator,
	U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator,
	U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator,
	U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator,
	U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator,
	U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator,
	U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D1Roll_mCA04F29B0D89B938CD0BAAC1DFA93BE0F3F5B93E,
	BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D2Roll_m6DF26307F1F6C8FA0639ACE4089FD048F6BF9A2B,
	BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D3Roll_m9B6955F3BB90BFDDE56D91D87B402A5DE1EE5363,
	BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D4Roll_m2B33EA25D83B1841421844288BE5697BA8617C42,
	BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_D0Roll_m5F8F5EB69DDAE7E2FF6FE27AF4088F801F9390D0,
	BigSpinScript_tEF4457DFA7D7831B80D6EA01FA3814BF23945599_CustomAttributesCacheGenerator_BigSpinScript_GlowOff_mE587F6A8033BA8E74F22CD50EB882D452825E1B2,
	U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46__ctor_mC43CF925E71EEEE6D5EC6B7B60A1FEB51CB7412C,
	U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_IDisposable_Dispose_m1737693F7494C54CCB147B2C7D01AB676A313302,
	U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25CC43358CCA05CD90AC1552FB25C9EA58ABE167,
	U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_Collections_IEnumerator_Reset_mD6C98D63697809D196D8C98C660229ED2855F683,
	U3CD1RollU3Ed__46_tF82A79554C5552DBEB115DF5FABBDA827039B9DB_CustomAttributesCacheGenerator_U3CD1RollU3Ed__46_System_Collections_IEnumerator_get_Current_m5B2EBA12F80D0C360E48312F206527B26AD7CF2A,
	U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48__ctor_m34F9DF1FCF70C68F78DF3313A0761A4F1E54DF5B,
	U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_IDisposable_Dispose_m2F082C8FB44BCDCE2A6681CD6B2F1352F6AD3FE9,
	U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m076D1448F288304DBDEFED7E4F0F9C7C11DC09C1,
	U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_Collections_IEnumerator_Reset_m47D792AD225263C1FDD16331EF799DC65A1EF2B1,
	U3CD2RollU3Ed__48_t795B22BE75E312C501665122C238F1973BFFA8E5_CustomAttributesCacheGenerator_U3CD2RollU3Ed__48_System_Collections_IEnumerator_get_Current_m141EFC4D4993D6B515760ACC5BC4C6DBBE1EB606,
	U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50__ctor_mF664E841A2123E9F87AF022D6B8B2746BB3F5771,
	U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_IDisposable_Dispose_m2D21D270EA4787BD1F24883F39726A18F1284F32,
	U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m740B9B023DDF18C7EB4E53897699BE7AD4E590C5,
	U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_Collections_IEnumerator_Reset_m5934D42F0AFC9E361057795639E9450C6D51EBAB,
	U3CD3RollU3Ed__50_t7D33C49C129EA774FE910EE71BCF052FF4FA97B0_CustomAttributesCacheGenerator_U3CD3RollU3Ed__50_System_Collections_IEnumerator_get_Current_m2875529B1353D233B2EF3053770BDC98121A8D2C,
	U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52__ctor_m39EA26456538681B5B450FBC0A45048B685EA443,
	U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_IDisposable_Dispose_mFC12D23B3C5630D31B5E09668CD03DC698664095,
	U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC687A2B724C9A95AED3B5409F928EFAE5D991FB3,
	U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_Collections_IEnumerator_Reset_m199DF633E2126455CD22DA25E135939F99743630,
	U3CD4RollU3Ed__52_t7D162A7B5DB4107D0BA93A936617A4BD8B6A21B6_CustomAttributesCacheGenerator_U3CD4RollU3Ed__52_System_Collections_IEnumerator_get_Current_mCD7E9315D3909443AD249ACB90185137E3371D09,
	U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54__ctor_m4ED4B2248BD7134DAFB983D43FF6CD0F25D7415E,
	U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_IDisposable_Dispose_mA1C2AC44AD18E50670E13C5A5F13B4D5FA6B911F,
	U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4EFBE0283E4E16688B37B5E3CD7C5B6000CBFD19,
	U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_Collections_IEnumerator_Reset_m4F484A1845BF4C7E1B1A1E62EAA91F33AD2F7BDB,
	U3CD0RollU3Ed__54_t2815A809C1B5EAD18A7085528D02BA2CFE1AAC41_CustomAttributesCacheGenerator_U3CD0RollU3Ed__54_System_Collections_IEnumerator_get_Current_m910B70EA328B0A578960A27CB09F1453BE4EE6ED,
	U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57__ctor_m6FCC904E99133E35852DAE3A53C7784718016147,
	U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_IDisposable_Dispose_mA4FA503FC230E455AC4732FE3BC893FF4A2BAC5B,
	U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC66749C532249391E6CDB39582252CF44A2C43A,
	U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_Collections_IEnumerator_Reset_m596CC7F9EDC1EFD59D0D8325BCC857D6DE200D6E,
	U3CGlowOffU3Ed__57_tF75068B8446A658DF37B402E9D64C9A45E2B06A3_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__57_System_Collections_IEnumerator_get_Current_m08E7E10DA481D334BC87188BA8ED3D3131F7BC31,
	BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D1Roll_m36B98E6620CAAFC4743AEAECBADA3E0D9F3481B3,
	BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D2Roll_m1CD4D1AABF35B11A56007ED0FEFDAA6186B3A17F,
	BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D3Roll_m299750C03D7D84B7453F829BE5A4DEF125162237,
	BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D4Roll_mB6840223079D89D43E6C3F3088941B7C61F832B4,
	BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_D0Roll_m57D44D8EE15528C0492A1FE37FC02A90E34BAFC4,
	BigSpinScriptOld_t9815E77C9ECD1FC0610F5073C9F160C54B4784CE_CustomAttributesCacheGenerator_BigSpinScriptOld_GlowOff_m2CB22540441C473BDD23D617F05437AB4515D20E,
	U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40__ctor_mE5B29F6FEDB78A1E0DFDED44AE446990ADEB1BCF,
	U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_IDisposable_Dispose_mA01E50BE7CA5932FCAB4D9B3925F9C5E31D19B24,
	U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A8E091583305BE7D0124F3FB1722662A00250B8,
	U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_Collections_IEnumerator_Reset_m1BE81F253360CC349978ADF7D371F8781B9759C9,
	U3CD1RollU3Ed__40_tCE08E8AAAC484F30365A7B7CA4C2BDE22FD7EBB2_CustomAttributesCacheGenerator_U3CD1RollU3Ed__40_System_Collections_IEnumerator_get_Current_mAB9C9E2BE53BD8720D526B9C3928A7B37DE6CB00,
	U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42__ctor_mF657CCBFB546DFE901444AFCD59CA8D0FA503DC6,
	U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_IDisposable_Dispose_m00ADC5355615BC4F0CEE048646265A9A6310E3B2,
	U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2982DE58894FF46A83F4EDBC671AEA19EFEBB5C4,
	U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_Collections_IEnumerator_Reset_m074EF937CFD67DDDC7BC91B0A0AF9502B12656C6,
	U3CD2RollU3Ed__42_t6A33C331E596716527656A73EA095B6B076A958D_CustomAttributesCacheGenerator_U3CD2RollU3Ed__42_System_Collections_IEnumerator_get_Current_mEA20832653AB5C5F26DA22A008419F6B03409E8A,
	U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44__ctor_m1D5E94C9DC00E9660C34D0EFAC5C8AE0B58FFA9E,
	U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_IDisposable_Dispose_m619B009D97C8ACA839AF65CF5B6D3CBFD8957B0A,
	U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C56EF30D6C5066EAC90CEAADB3EBC12553D7572,
	U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_Collections_IEnumerator_Reset_m3E26F9FC99711AA7C10EF99B01E97DD2AA0D560A,
	U3CD3RollU3Ed__44_tF2E265FEE96298DCD50134F2433F163E3889E897_CustomAttributesCacheGenerator_U3CD3RollU3Ed__44_System_Collections_IEnumerator_get_Current_m4BA2563E4BF763C5D132FD1BDF20D6B837538635,
	U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46__ctor_m1AA15588D444E70BD316D014362378636CCF35BC,
	U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_IDisposable_Dispose_m5819824F19F9D85E08066BC0F0B4C2A5AAD48AB6,
	U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m427EE9DD7919DE8F5DC2B50D56503F38F940676C,
	U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_Collections_IEnumerator_Reset_mC1B021472447AEB7BF0A196D77586D22ED108BCA,
	U3CD4RollU3Ed__46_t2BD84E6C2579C5DB0085E11534A83CF71C108CE5_CustomAttributesCacheGenerator_U3CD4RollU3Ed__46_System_Collections_IEnumerator_get_Current_mB8486BD58549DF44782F6BFAC395E4217812BD28,
	U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48__ctor_m5E16E053DC8BB428FAB155457A9EEECFC9AC4E9A,
	U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_IDisposable_Dispose_m3989A15A58B8D2B164540C77BF26100B11CF5A23,
	U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7879742364A48D269280EA4DA4B8DC4FFAED8E5C,
	U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_Collections_IEnumerator_Reset_m980A8717E95854ACBC300882A4E51CAAD3ECD1B4,
	U3CD0RollU3Ed__48_tF926314CAB7450F64C8688E9810980E07DEA380B_CustomAttributesCacheGenerator_U3CD0RollU3Ed__48_System_Collections_IEnumerator_get_Current_m9922F41CE0BBCD38679614BD4B5F54607A4DEB10,
	U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51__ctor_m6D157989D9F27FE90E8B55563A1DD3C251885DED,
	U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_IDisposable_Dispose_m5C5D08FC31FE0F5C814FDF21F4E12E5F744662CD,
	U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A9EAEEE4CADF12CE47D3C0BCD8C6078330567B8,
	U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_Collections_IEnumerator_Reset_mD58E627AAE1B363BEF1F32A6E77FD4FB938DFFEC,
	U3CGlowOffU3Ed__51_tB5B229607D33A2FE5887B84BAD05D46CA7373884_CustomAttributesCacheGenerator_U3CGlowOffU3Ed__51_System_Collections_IEnumerator_get_Current_mD29BC3F96D2B0455A326175D18A124818EFE5CCA,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
